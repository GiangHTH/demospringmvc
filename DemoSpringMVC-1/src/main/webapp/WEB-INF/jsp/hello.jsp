<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello</title>
</head>
<body>
	<h2>Hello!</h2>
	<h3>My name is ${firstName } ${lastName }</h3>
	<p>Gender: ${gender }</p>
	<p>Country: ${country }</p>
	<p>Visited Country:</p>
	<ul>
		<c:forEach items="${visitedCountry }" var="vcountry">
			<li>${vcountry }</li>
		</c:forEach>
	</ul>
	<p>Message: ${mess }</p>
</body>
</html>