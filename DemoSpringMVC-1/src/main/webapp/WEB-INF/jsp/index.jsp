<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Index</title>
</head>
<body>
	<h2>Submit your information</h2>
	<hr><br/>
	<form:form action="hello" commandName="info">
		FirstName: <form:input path="firstName"/><p/> <!-- value entered here will "setFirstName" into Information class via "info" -->
		LastName: <form:input path="lastName"/><p/><!-- value entered here will "setLastName" into Information class via "info" -->
		Gender: <form:radiobuttons path="gender" items="${gender }"/><p/><!-- value choosed here will "setGender" into Information class via "info" -->
		Country: <form:select path="country" items="${countries }"><!-- value entered here will "setCountry" into Information class via "info" -->
				 </form:select> <p/>
		Visited Country:<form:checkboxes items="${visitedCountries }" path="visitedCountry"/><!-- value entered here will "setVisitedCountry" into Information class via "info" -->
			<p/>
		Message: <form:textarea path="message" placeholder="give me a message..."/><p/><!-- value entered here will "setMessage" into Information class via "info" -->
		<input type="submit" value="submit">
	</form:form>
</body>
</html>