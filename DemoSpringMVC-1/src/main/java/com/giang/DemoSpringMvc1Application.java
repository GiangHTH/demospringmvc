package com.giang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class DemoSpringMvc1Application{

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringMvc1Application.class, args);
	}
}
