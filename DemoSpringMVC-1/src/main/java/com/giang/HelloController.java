package com.giang;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

	// get info object from index.jsp using @ModelAttribute
	@RequestMapping(value = "/hello", method = RequestMethod.POST)
	public ModelAndView HelloWorld(@ModelAttribute("info") Information info) {
		ModelAndView model = new ModelAndView("hello");
		model.addObject("firstName", info.getFirstName());
		model.addObject("lastName", info.getLastName());
		model.addObject("gender", info.getGender());
		model.addObject("country", info.getCountry());
		model.addObject("mess", info.getMessage());
		model.addObject("visitedCountry", info.getVisitedCountry());
		return model;
	}

	// send info object to index.jsp page. it will set value there.
	@RequestMapping("/")
	public ModelAndView HomePage() {
		ModelAndView model = new ModelAndView("index", "info", new Information());
		Map<String, String> gender = new HashMap<String, String>();
		gender.put("Male", "Male");
		gender.put("Female", "Female");
		model.addObject("gender", gender);
		
		//list country
		Map<String, String> countries = new HashMap<String, String>();
		countries.put("VietNam", "VietNam");
		countries.put("American", "American");
		countries.put("Singapore", "Singapore");
		countries.put("Japan", "Japan");
		countries.put("England", "England");
		countries.put("France", "France");
		model.addObject("countries", countries);
		
		//list visited country
		Map<String, String> visitedCountries = new HashMap<String, String>();
		visitedCountries.put("VietNam", "VietNam");
		visitedCountries.put("USA", "USA");
		visitedCountries.put("Singapore", "Singapore");
		visitedCountries.put("Indo", "Indo");
		visitedCountries.put("China", "China");
		visitedCountries.put("Thailand", "Thailand");
		visitedCountries.put("Russia", "Russia");
		model.addObject("visitedCountries", visitedCountries);
		return model;
	}
}
